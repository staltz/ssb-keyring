# ssb-keyring

A persistence store for encryption keys for scuttlebutt.
This module was extracted from `ssb-tribes`


## Usage

```js
const keyRing = require('ssb-keyring')
const ssbKeys = require('ssb-keys')

const feedKeys = ssbKeys.generate()

keyRing('`/tmp/keyring-demo-${Date.now()}`, feedKeys, (err, keys) => {

  const myGroupKeys = keys.group.list()
    .map(groupId => keys.group.get(groupId))
})
```

## API

Note that where possible all methods are synchronous.
This is so that unboxers never have to wait for IO before pulling keys.

```
const api = {
  group: {
    register,          // async
    registerAuthors,   // async

    get,
    list,
    listAuthors,
  },
  processAddMember,    // async
  processPOBox,        // async
  author: {
    groups,
    groupKeys,
    sharedDMKey
  },
  ownKeys,
  poBox: {
    register          // async
    get,
    list,
  },
  close               // async
}

```

### `keyRing(path, ssbKeys, cb)`

where
- `path` *String* to location your keys will be persisted to on disk
- `ssbKeys` *Object* your public and private feed keys, used for calculating shared DM keys with other authors

---

### `keys.group.register(groupId, info, cb)`

where
- `groupId` *String* a cloaked messageId which identifies the group
- `info` *Object*:
    - `info.key` is the group encryption key
    - `info.scheme` scheme of that encryption key (optional, there is only one option at the moment which we default to)
    - `info.root` the messageId of the `group/init` message

### `keys.group.registerAuthors(groupId, [feedId, ...], cb)`

### `keys.group.get(groupId) => info`
### `keys.group.list(groupId) => [groupId, ...]`
### `keys.group.listAuthors(groupId) => [feedId, ...]`

---

### `keys.processAddMember({ groupId, groupKey, root, authors }, cb)`

A convenience method for handling `group/add-member` messages. It takes care of :
- making sure group details are registered
- making sure authors are associated with groups
- telling us which authors are new (so that we can elsewhere trigger index rebuilds)

It calls back with an array `[feedId, ...]` of authors which are new (to our knowledge) to the group.
This list is useful because it lets you know if you need to rebuild your database indexes or not.

### `keys.processPOBox({ poBoxId, poBoxKey }, cb)`

A convenience method for handling `group/po-box` messages. It takes care of :
- making sure P.O. Box details are registered
- telling us if this is new to us (so that we can elsewhere trigger index rebuilds)

It calls back with a Boolean:
- `true` = new to use (so you should probably re-index)
- `false = not new to us

---

### `keys.author.groups(feedId) => [groupId, ...]`
### `keys.author.groupKeys(feedId) => [keySchema, ...]`

convenience method, could probably deprecate and have people map with `keys.group.get`

### `keys.author.sharedDmKey(feedId) => keySchema`

calculates a shared DM key between your `ssbKeys` and `feedId`, returns `{ key, scheme }`

---

### `keys.ownKeys() => [keySchema...]`

---

### `keys.poBox.register(poBoxId, info, cb)`

- `info` *Object*
    - `info.key` the private part of a diffie-hellman key
    - `info.scheme` the scheme associated with that key (currently optional)

### `keys.poBox.get(poBoxId) => info`
### `keys.poBox.list() => [poBoxId, ...]`

---

### `keys.close(cb)`


_apologies the docs are brief, please open an issue or PR if you'd like more detail_
