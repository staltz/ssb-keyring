const { promisify } = require('util')
const mkdirp = require('mkdirp')
const Level = require('level')
const charwise = require('charwise')

const Group = require('./models/group')
const Membership = require('./models/membership')
const Own = require('./models/dm-self')
const DM = require('./models/dm-other')
const POBox = require('./models/po-box')

const infoEncoding = require('./info-encoding')
const { isSameKey } = require('./util')

module.exports = function KeyStore (path, ssbKeys, onReady) {
  if (onReady === undefined) return promisify(KeyStore)(path, ssbKeys)

  mkdirp.sync(path)
  const level = Level(path, {
    keyEncoding: charwise,
    valueEncoding: infoEncoding
  })

  const group = Group(level, ssbKeys)
  const membership = Membership(level, ssbKeys)
  const own = Own(level, ssbKeys)
  const dm = DM(level, ssbKeys)
  const poBox = POBox(level, ssbKeys)

  /* META methods - span GROUP/ MEMBERSHIP */
  function getAuthorGroupKeys (authorId) {
    return membership.getAuthorGroups(authorId)
      .map(groupId => {
        const info = group.get(groupId)
        if (!info) {
          throw new Error(`unknown group ${groupId}`)
        }

        return info
      })
  }

  function processAddMember ({ groupId, groupKey, root, authors }, cb) {
    const record = group.get(groupId)

    if (record == null) {
      return group.register(groupId, { key: groupKey, root }, (err) => {
        if (err) return cb(err)

        membership.register(groupId, authors, (err) => {
          if (err) return cb(err)
          cb(null, authors)
        })
      })
    }

    if (!isSameKey(record.key, groupKey)) {
      // we see this is comparing a string + Buffer!
      // because during persistence we map key > Buffer
      return cb(new Error(`groupId ${groupId} already registered with a different groupKey`))
    }
    if (record.root !== root) {
      return cb(new Error(`groupId ${groupId} already registered with a different root`))
    }

    const authorsNotInGroup = authors.filter((author) => {
      return !membership
        .getAuthorGroups(author)
        .includes(groupId)
    })
    membership.register(groupId, authorsNotInGroup, (err) => {
      if (err) return cb(err)
      cb(null, authorsNotInGroup)
    })
  }

  function processPOBox ({ poBoxId, poBoxKey }, cb) {
    const record = poBox.get(poBoxId)

    if (record == null) {
      return poBox.register(poBoxId, { key: poBoxKey }, (err) => {
        if (err) return cb(err)
        cb(null, true)
      })
    }

    if (!isSameKey(record.key, poBoxKey)) {
      // we see this is comparing a string + Buffer!
      // because during persistence we map key > Buffer
      return cb(new Error(`poBoxId ${poBoxId} already registered with a different key`))
    }
    cb(null, false)
  }

  /* API */
  const api = {
    group: {
      register: group.register, // ------------------ async
      registerAuthors: membership.register, // ------ async

      get: group.get,
      list: group.list,
      listAuthors: membership.getGroupAuthors
    },
    processAddMember, // ---------------------------- async
    processPOBox, // -------------------------------- async
    author: {
      groups: membership.getAuthorGroups,
      groupKeys: getAuthorGroupKeys,
      sharedDMKey: dm
    },
    ownKeys: own.list,
    poBox: {
      register: poBox.register, // ------------------ async

      get: poBox.get,
      list: poBox.list
    },
    close: level.close.bind(level) // --------------- async
  }

  /* loads persisted states into cache */
  group.load(err => {
    if (err) return onReady(err)
    membership.load(err => {
      if (err) return onReady(err)
      own.load(err => {
        if (err) return onReady(err)
        poBox.load(err => {
          if (err) return onReady(err)

          onReady(null, api)
        })
      })
    })
  })
}
