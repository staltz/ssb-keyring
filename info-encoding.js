const { keySchemes } = require('private-group-spec')
const { toBuffer, isObject, isString, isBuffer } = require('./util')

const keyEncoding = {
  encode (buf) {
    if (typeof buf === 'string') return buf
    if (isBuffer(buf)) return buf.toString('base64')

    throw new Error(`expected type Buffer|String, got ${typeof buf}`)
  },
  decode: toBuffer
}

const schemeEncoding = {
  encode (str) {
    switch (str) {
      case keySchemes.private_group: return 1
      case keySchemes.feed_id_self: return 2
      case keySchemes.po_box: return 3
      default: return str
    }
  },
  decode (int) {
    switch (int) {
      case 1: return keySchemes.private_group
      case 2: return keySchemes.feed_id_self
      case 3: return keySchemes.po_box
      default: return int
    }
  }
}

module.exports = {
  encode (info) {
    if (isString(info)) return JSON.stringify(info)
    if (isObject(info)) {
      const output = { ...info }
      if (info.key) output.key = keyEncoding.encode(info.key)
      if (info.scheme) output.scheme = schemeEncoding.encode(info.scheme)

      return JSON.stringify(output)
    }

    throw new Error('unable to encode info')
  },
  decode (str) {
    const info = JSON.parse(str)

    if (info.key) info.key = keyEncoding.decode(info.key)
    if (info.scheme) info.scheme = schemeEncoding.decode(info.scheme)
    return info
  },
  buffer: false,
  type: 'keystore-info-encoding'
}
