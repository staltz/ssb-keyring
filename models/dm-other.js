const { directMessageKey } = require('ssb-private-group-keys')

module.exports = function DMOther (db, ssbKeys) {
  const buildDMKey = directMessageKey.easy(ssbKeys)

  const cache = new Map([])
  // maps feedId > { key, scheme: SharedDMKey }
  // TODO - make this a hashlru cache to bound its size?

  return function getSharedDMKey (feedId) {
    if (feedId === ssbKeys.id) {
      throw new Error('not allowed to make a sharedDMKey for your own feedId, for that use ownKeys')
    }
    if (!cache.has(feedId)) cache.set(feedId, buildDMKey(feedId))

    if (cache.size > 4096) {
      console.log(`Warning, your shareDMKey cache now contains ${cache.size} entries`)
    }

    return cache.get(feedId)
  }
}
