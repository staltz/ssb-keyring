const pull = require('pull-stream')
const { read } = require('pull-level')

const { keySchemes } = require('private-group-spec')
const { SecretKey } = require('ssb-private-group-keys')

const OWN = 'own_key'

module.exports = function DMSelf (db, ssbKeys) {
  let cache // Array of entries

  return {
    // TODO add register
    load (cb) {
      readPersisted((err, keys) => {
        if (err) return cb(err)

        cache = keys
        if (keys.length === 0) create((err) => err ? cb(err) : cb(null))
        else cb(null)
      })
    },

    create,

    list () {
      return cache.map(key => {
        return {
          key,
          scheme: keySchemes.feed_id_self
        }
      })
    }
  }

  function create (cb) {
    const key = new SecretKey().toBuffer()

    cache = [...cache, key]
    db.put([OWN, ssbKeys.id, Date.now()], { key }, cb)
  }

  function readPersisted (cb) {
    pull(
      read(db, {
        lt: [OWN + '~', undefined, undefined], // "own~" is just above "own" in charwise sort
        gt: [OWN, null, null]
      }),
      pull.map(({ key, value }) => value.key),
      pull.collect((err, keys) => {
        if (err) return cb(err)

        cb(null, keys)
      })
    )
  }
}
