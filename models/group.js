const pull = require('pull-stream')
const { read } = require('pull-level')
const { keySchemes } = require('private-group-spec')
const { isMsg, isCloakedMsg: isGroup } = require('ssb-ref')
const na = require('sodium-native')

const { toBuffer } = require('../util')

const GROUP = 'group'

module.exports = function Group (db, ssbKeys) {
  let cache // maps groupId > group.info

  return {
    load (cb) {
      readPersisted((err, groups) => {
        if (err) return cb(err)
        cache = groups
        cb(null)
      })
    },

    register (groupId, info, cb) {
      if (cache[groupId]) return cb(new Error(`group ${groupId} already registered, cannot register twice`))
      if (!isGroup(groupId)) return cb(new Error(`expected a groupId, got ${groupId}`))
      if (!isMsg(info.root)) return cb(new Error(`expects root got ${info.root}`))
      // TODO check info.scheme is valid in some way?
      if (!info.scheme) info.scheme = keySchemes.private_group

      try {
        // convert to 32 Byte buffer
        info.key = toBuffer(info.key, na.crypto_secretbox_KEYBYTES)
      } catch (e) { return cb(e) }

      cache[groupId] = info
      db.put([GROUP, groupId, Date.now()], info, cb)
    },
    get (groupId) {
      return cache[groupId]
    },
    list () {
      return Object.keys(cache)
    }
  }

  function readPersisted (cb) {
    pull(
      read(db, {
        lt: [GROUP + '~', undefined, undefined], // "group~" is just above "group" in charwise sort
        gt: [GROUP, null, null]
      }),
      pull.map(({ key, value: info }) => {
        const [_, groupId, createdAt] = key // eslint-disable-line
        return { [groupId]: info }
      }),
      pull.collect((err, pairs) => {
        if (err) return cb(err)
        cb(null, Object.assign({}, ...pairs))
      })
    )
  }
}
