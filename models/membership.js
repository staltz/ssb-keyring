const pull = require('pull-stream')
const { read } = require('pull-level')
const { isFeed, isCloakedMsg: isGroup } = require('ssb-ref')

const MEMBER = 'member'

module.exports = function Membership (db, ssbKeys) {
  // membership stores the joins of a group + author
  let cache // - maps authorId > Set([groupId])

  return {
    load (cb) {
      readPersisted((err, groups) => {
        if (err) return cb(err)
        cache = groups
        cb(null)
      })
    },

    register (groupId, authors, cb) {
      if (!isGroup(groupId)) return cb(new Error(`expected a groupId, got ${groupId}`))
      if (!authors.every(isFeed)) return cb(new Error(`expected [feedId], got ${authors}`))

      pull(
        pull.values(authors),
        pull.asyncMap((authorId, cb) => registerAuthor(groupId, authorId, cb)),
        pull.collect((err, arr) => cb(err))
      )
    },

    getAuthorGroups (authorId) {
      return Array.from(cache[authorId] || [])
    },

    getGroupAuthors (groupId) {
      return Object.keys(cache)
        .filter(authorId => cache[authorId].has(groupId))
    }
  }

  function registerAuthor (groupId, authorId, cb) {
    if (!cache[authorId]) cache[authorId] = new Set()

    cache[authorId].add(groupId)
    db.put([MEMBER, authorId, groupId], groupId, cb)
  }

  function readPersisted (cb) {
    pull(
      read(db, {
        lt: [MEMBER + '~', undefined, undefined], // "member~" is just above "member" in charwise sort
        gt: [MEMBER, null, null]
      }),
      pull.map(({ key, value: groupId }) => {
        const [_, authorId] = key // eslint-disable-line
        return { authorId, groupId }
      }),
      pull.collect((err, pairs) => {
        if (err) return cb(err)

        const list = pairs.reduce((acc, { authorId, groupId }) => {
          if (!acc[authorId]) acc[authorId] = new Set()

          acc[authorId].add(groupId)
          return acc
        }, {})

        cb(null, list)
      })
    )
  }
}
