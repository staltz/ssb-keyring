const pull = require('pull-stream')
const { read } = require('pull-level')
const na = require('sodium-native')
const { keySchemes } = require('private-group-spec')

const { toBuffer, isPOBoxId } = require('../util')

const POBOX = 'pobox'

module.exports = function POBox (db, ssbKeys) {
  let cache // maps groupId > group.info

  return {
    load (cb) {
      readPersisted((err, data) => {
        if (err) return cb(err)
        cache = data
        cb(null)
      })
    },

    register (poBoxId, info, cb) {
      if (cache[poBoxId]) return cb(new Error(`already registered poBoxId ${poBoxId}, cannot register twice`))
      if (!isPOBoxId(poBoxId)) return cb(new Error(`expected a poBoxId, got ${poBoxId}`))
      if (!info.key) return cb(new Error('expected info.key'))
      // TODO <<< store public/secret keys in BFE format, along with ID in BFE format ???
      // TODO check info.scheme is valid in some way?
      if (!info.scheme) info.scheme = keySchemes.po_box

      try {
        // convert to 32 Byte buffer
        info.key = toBuffer(info.key, na.crypto_scalarmult_SCALARBYTES)
      } catch (e) { return cb(e) }

      cache[poBoxId] = info
      db.put([POBOX, poBoxId, Date.now()], info, cb)
    },
    get (poBoxId) {
      return cache[poBoxId]
    },
    list () {
      return Object.keys(cache)
    }
  }

  function readPersisted (cb) {
    pull(
      read(db, {
        lt: [POBOX + '~', undefined, undefined], // "group~" is just above "group" in charwise sort
        gt: [POBOX, null, null]
      }),
      pull.map(({ key, value: info }) => {
        const [_, poBoxId, createdAt] = key // eslint-disable-line
        return { [poBoxId]: info }
      }),
      pull.collect((err, pairs) => {
        if (err) return cb(err)
        cb(null, Object.assign({}, ...pairs))
      })
    )
  }
}
