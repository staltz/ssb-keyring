const test = require('tape')
const { promisify: p } = require('util')
const { generate } = require('ssb-keys')

const { directMessageKey } = require('ssb-private-group-keys')
const KeyStore = require('../')
const { tmpPath } = require('./helpers')

test('direct messages', t => {
  const myKeys = generate() // some ssb-keys

  const tests = [
    async () => {
      const DESCRIPTION = 'author.sharedDMKey'
      // this is also tested by the "no groups" case above

      const keyStore = await p(KeyStore)(tmpPath(), myKeys)

      const otherKeys = generate() // some other feed

      const expectedDMKey = directMessageKey.easy(myKeys)(otherKeys.id)

      t.deepEqual(
        keyStore.author.sharedDMKey(otherKeys.id),
        expectedDMKey,
        DESCRIPTION
      )
      keyStore.close()
    },

    async () => {
      const DESCRIPTION = 'ownKeys'

      const storePath = tmpPath()
      const keyStore = await p(KeyStore)(storePath, myKeys)

      const keys = keyStore.ownKeys()
      t.equal(keys.length, 1, DESCRIPTION)

      keyStore.close()
    },

    async () => {
      const DESCRIPTION = 'ownKeys (persisted)'

      const storePath = tmpPath()
      const keyStore = await p(KeyStore)(storePath, myKeys)

      const keys = keyStore.ownKeys()

      keyStore.close((err) => {
        if (err) throw err
        KeyStore(storePath, myKeys, (err, keyStore) => {
          if (err) throw err

          t.deepEqual(keyStore.ownKeys(), keys, DESCRIPTION)
          keyStore.close()
        })
      })
    }
  ]

  const toRun = tests.length

  t.plan(toRun)
  tests
    .slice(0, toRun)
    .forEach(i => i())
})
