const test = require('tape')
const { promisify: p } = require('util')
const { keySchemes } = require('private-group-spec')
const { generate } = require('ssb-keys')

const KeyStore = require('../')
const { tmpPath, GroupKey, GroupId, MsgId } = require('./helpers')

test('group', t => {
  const myKeys = generate() // some ssb-keys

  const tests = [
    async () => {
      const DESCRIPTION = 'group.register (error if bad groupId)'

      const keyStore = await p(KeyStore)(tmpPath(), myKeys)

      keyStore.group.register('junk', { key: GroupKey(), root: MsgId() }, (err) => {
        t.match(err && err.message, /expected a groupId/, DESCRIPTION)
        keyStore.close()
      })
    },

    async () => {
      const DESCRIPTION = 'group.register (error if bad key)'

      const keyStore = await p(KeyStore)(tmpPath(), myKeys)

      keyStore.group.register(GroupId(), { key: 'junk', root: MsgId() }, (err) => {
        t.match(err && err.message, /expected buffer of length 32/, DESCRIPTION)
        keyStore.close()
      })
    },

    async () => {
      const DESCRIPTION = 'group.register (error if bad root)'

      const keyStore = await p(KeyStore)(tmpPath(), myKeys)

      keyStore.group.register(GroupId(), { key: GroupKey(), root: 'dog' }, (err) => {
        t.match(err && err.message, /expects root/, DESCRIPTION)
        keyStore.close()
      })
    },

    async () => {
      const DESCRIPTION = 'group.register (error if try to double-add)'

      const keyStore = await p(KeyStore)(tmpPath(), myKeys)

      const groupId = GroupId()
      const key = GroupKey()
      const root = MsgId()

      keyStore.group.register(groupId, { key, root }, (_, data) => {
        keyStore.group.register(groupId, { key, root }, (err) => {
          t.match(err && err.message, /already registered/, DESCRIPTION)
          keyStore.close()
        })
      })
    },

    async () => {
      const DESCRIPTION = 'group.register + group.get'

      const keyStore = await p(KeyStore)(tmpPath(), myKeys)

      const groupId = GroupId()
      const key = GroupKey()
      const root = MsgId()

      keyStore.group.register(groupId, { key, root }, (_, data) => {
        const info = keyStore.group.get(groupId)
        t.deepEqual(info, { key, root, scheme: keySchemes.private_group }, DESCRIPTION)

        keyStore.close()
      })
    },

    async () => {
      const DESCRIPTION = 'group.register (persisted)'

      const storePath = tmpPath()
      const keyStore = await KeyStore(storePath, myKeys)

      const groupId = GroupId()
      const key = GroupKey()
      const root = MsgId()

      keyStore.group.register(groupId, { key, root }, (err, data) => {
        if (err) throw err

        keyStore.close((err) => {
          if (err) throw err

          KeyStore(storePath, myKeys, (err, keyStore) => {
            if (err) throw err

            const persisted = keyStore.group.list().reduce(
              (acc, groupId) => {
                return {
                  ...acc,
                  [groupId]: keyStore.group.get(groupId)
                }
              },
              {}
            )

            t.deepEqual(
              persisted,
              { [groupId]: { key, root, scheme: keySchemes.private_group } },
              DESCRIPTION
            )

            keyStore.close()
          })
        })
      })
    }
  ]

  const toRun = tests.length

  t.plan(toRun)
  tests
    .slice(0, toRun)
    .forEach(i => i())
})
