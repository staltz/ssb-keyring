const test = require('tape')
const { generate } = require('ssb-keys')
const KeyStore = require('../')
const { tmpPath } = require('./helpers')

test('init', t => {
  const myKeys = generate() // some ssb-keys

  const tests = [
    () => {
      const DESCRIPTION = 'callback init'

      KeyStore(tmpPath(), myKeys, (err, keyStore) => {
        if (err) throw err
        t.true(keyStore.ownKeys(), DESCRIPTION) // some simple method to verify working
        keyStore.close()
      })
    },

    async () => {
      const DESCRIPTION = 'promise init'

      const keyStore = await KeyStore(tmpPath(), myKeys)
      t.true(keyStore.ownKeys(), DESCRIPTION) // some simple method to verify working
      keyStore.close()
    }
  ]

  const toRun = tests.length

  t.plan(toRun)
  tests
    .slice(0, toRun)
    .forEach(i => i())
})
