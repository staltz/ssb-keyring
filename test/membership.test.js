/* eslint-disable camelcase */

const test = require('tape')
const { promisify: p } = require('util')
const { keySchemes } = require('private-group-spec')
const { generate } = require('ssb-keys')

const KeyStore = require('../')
const { tmpPath, GroupKey, GroupId, MsgId } = require('./helpers')

test('membership', t => {
  const myKeys = generate() // some ssb-keys

  const tests = [
    async () => {
      const DESCRIPTION = 'group.registerAuthors (not a feedId errors)'

      const keyStore = await p(KeyStore)(tmpPath(), myKeys)
      const groupId_A = GroupId()

      keyStore.group.registerAuthors(groupId_A, ['@mix'], (err) => {
        // console.log('ERRROR', err)
        t.true(err, DESCRIPTION)
        keyStore.close()
      })
    },

    async () => {
      const DESCRIPTION = 'group.registerAuthors + author.groups'

      const keyStore = await p(KeyStore)(tmpPath(), myKeys)
      const groupId_A = GroupId()
      const groupId_B = GroupId()
      const keyA = GroupKey()
      const keyB = GroupKey()
      const root_A = MsgId()
      const root_B = MsgId()

      const authorId = generate().id

      keyStore.group.register(groupId_A, { key: keyA, root: root_A }, (_, __) => {
        keyStore.group.register(groupId_B, { key: keyB, root: root_B }, (_, __) => {
          keyStore.group.registerAuthors(groupId_A, [authorId], (_, __) => {
            keyStore.group.registerAuthors(groupId_B, [authorId], (_, __) => {
              const groups = keyStore.author.groups(authorId)
              t.deepEqual(groups, [groupId_A, groupId_B], DESCRIPTION)

              keyStore.close()
            })
          })
        })
      })
    },

    async () => {
      const DESCRIPTION = 'group.registerAuthors + author.groupKeys'

      const keyStore = await p(KeyStore)(tmpPath(), myKeys)
      const groupId_A = GroupId()
      const groupId_B = GroupId()
      const keyA = GroupKey()
      const keyB = GroupKey()
      const root_A = MsgId()
      const root_B = MsgId()

      const authorId = generate().id

      keyStore.group.register(groupId_A, { key: keyA, root: root_A }, (err, __) => {
        if (err) throw err
        keyStore.group.register(groupId_B, { key: keyB, root: root_B }, (err, __) => {
          if (err) throw err
          keyStore.group.registerAuthors(groupId_A, [authorId], (_, __) => {
            keyStore.group.registerAuthors(groupId_B, [authorId], (_, __) => {
              const keys = keyStore.author.groupKeys(authorId)
              const expected = [
                { key: keyA, root: root_A, scheme: keySchemes.private_group },
                { key: keyB, root: root_B, scheme: keySchemes.private_group }
              ]
              t.deepEqual(keys, expected, DESCRIPTION)

              keyStore.close()
            })
          })
        })
      })
    },

    async () => {
      const DESCRIPTION = 'author.groupKeys (no groups)'

      const keyStore = await p(KeyStore)(tmpPath(), myKeys)

      const groupId_A = GroupId()
      const groupId_B = GroupId()
      const keyA = GroupKey()
      const keyB = GroupKey()
      const root_A = MsgId()
      const root_B = MsgId()

      const authorId = generate().id
      const otherAuthorId = generate().id

      keyStore.group.register(groupId_A, { key: keyA, root: root_A }, (err, __) => {
        if (err) throw err
        keyStore.group.register(groupId_B, { key: keyB, root: root_B }, (err, __) => {
          if (err) throw err
          keyStore.group.registerAuthors(groupId_A, [authorId], (_, __) => {
            keyStore.group.registerAuthors(groupId_B, [authorId], (_, __) => {
              const keys = keyStore.author.groupKeys(otherAuthorId)
              const expected = []
              t.deepEqual(keys, expected, DESCRIPTION)

              keyStore.close()
            })
          })
        })
      })
    },

    async () => {
      const DESCRIPTION = 'author.groupKeys (persistence)'

      const storePath = tmpPath()
      const keyStore = await p(KeyStore)(storePath, myKeys)

      const groupId_A = GroupId()
      const groupId_B = GroupId()
      const keyA = GroupKey()
      const keyB = GroupKey()
      const root_A = MsgId()
      const root_B = MsgId()

      const authorId = generate().id

      keyStore.group.register(groupId_A, { key: keyA, root: root_A }, (err, __) => {
        if (err) throw err
        keyStore.group.register(groupId_B, { key: keyB, root: root_B }, (err, __) => {
          if (err) throw err
          keyStore.group.registerAuthors(groupId_A, [authorId], (_, __) => {
            keyStore.group.registerAuthors(groupId_B, [authorId], (_, __) => {
              keyStore.close(() => {
                // start new keyStore with same path, and this time wait for onReady
                KeyStore(storePath, myKeys, (err, newKeyStore) => {
                  if (err) throw err
                  // then check to see if keys were persisted

                  const keys = newKeyStore.author.groupKeys(authorId)
                  const expected = [
                    { key: keyA, root: root_A, scheme: keySchemes.private_group },
                    { key: keyB, root: root_B, scheme: keySchemes.private_group }
                  ]
                  const compare = (a, b) => a.key < b.key ? -1 : 1
                  t.deepEqual(keys.sort(compare), expected.sort(compare), DESCRIPTION)

                  newKeyStore.close()
                })
              })
            })
          })
        })
      })
    }
  ]

  const toRun = tests.length

  t.plan(toRun)
  tests
    .slice(0, toRun)
    .forEach(i => i())
})
