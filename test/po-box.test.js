/* eslint-disable camelcase */

const test = require('tape')
const { promisify: p } = require('util')
const { keySchemes } = require('private-group-spec')
const { generate } = require('ssb-keys')

const KeyStore = require('../')
const { tmpPath, POBox } = require('./helpers')
const myKeys = generate()

test('P.O. Box', t => {
  const tests = [
    async () => {
      const DESCRIPTION = 'poBox.register'

      const keyStore = await p(KeyStore)(tmpPath(), myKeys)
      const { id, key } = POBox()

      const info = {
        key,
        scheme: keySchemes.po_box
      }

      keyStore.poBox.register(id, info, (err) => {
        t.error(err, DESCRIPTION)
        keyStore.close()
      })
    },

    async () => {
      const DESCRIPTION = 'poBox.register (error, not poBoxId)'

      const keyStore = await p(KeyStore)(tmpPath(), myKeys)
      const { key } = POBox()

      const info = {
        key,
        scheme: 'dog'
      }

      keyStore.poBox.register('dog', info, (err) => {
        t.true(err, DESCRIPTION)
        keyStore.close()
      })
    },
    async () => {
      const DESCRIPTION = 'poBox.register (error, no info.key)'

      const keyStore = await p(KeyStore)(tmpPath(), myKeys)
      const { id } = POBox()

      const info = {
        // key
        scheme: 'dog'
      }

      keyStore.poBox.register(id, info, (err) => {
        t.true(err, DESCRIPTION)
        keyStore.close()
      })
    },
    // async () => {
    //   const DESCRIPTION = 'poBox.register (error, no info.scheme)'

    //   const keyStore = await p(KeyStore)(tmpPath(), myKeys)
    //   const { id, key } = POBox()

    //   const info = {
    //     key
    //     // scheme
    //   }

    //   keyStore.poBox.register(id, info, (err) => {
    //     t.true(err, DESCRIPTION)
    //     keyStore.close()
    //   })
    // },

    async () => {
      const DESCRIPTION = 'poBox.register + poBox.get'

      const keyStore = await p(KeyStore)(tmpPath(), myKeys)
      const { id, key } = POBox()

      const info = {
        key,
        scheme: keySchemes.po_box
      }

      keyStore.poBox.register(id, info, (err) => {
        if (err) throw err

        const data = keyStore.poBox.get(id)
        t.deepEqual(data, info, DESCRIPTION)
        keyStore.close()
      })
    },

    async () => {
      const DESCRIPTION = 'poBox.register (persisted)'

      const path = tmpPath()
      const keyStore = await p(KeyStore)(path, myKeys)
      const { id, key } = POBox()

      const info = {
        key,
        scheme: keySchemes.po_box
      }

      keyStore.poBox.register(id, info, (err) => {
        if (err) throw err
        keyStore.close(err => {
          if (err) throw err
          KeyStore(path, myKeys, (err, keyStore) => {
            if (err) throw err

            t.deepEqual(
              keyStore.poBox.list(),
              [id],
              DESCRIPTION
            )
            keyStore.close()
          })
        })
      })
    }

    // TODO check that scheme always comes out,
    // consider introducing a "code" for different schemes i.e. 0 = sadsasdasd (to saved space)
    //
    // allow custom schemes?
  ]

  const toRun = tests.length

  t.plan(toRun)
  tests
    .slice(0, toRun)
    .forEach(i => i())
})
