const bfe = require('ssb-bfe')
const na = require('sodium-native')

function toBuffer (thing, length) {
  const buf = Buffer.isBuffer(thing)
    ? thing
    : Buffer.from(thing, 'base64')

  if (length && buf.length !== length) {
    throw new Error(`expected buffer of length ${length} bytes, got ${buf.length}`)
  }

  return buf
}

const POBOX_TF = bfe.toTF('identity', 'po-box')
function isPOBoxId (id) {
  try {
    const bfeId = bfe.encode(id)

    return (
      bfeId.slice(0, 2).equals(POBOX_TF) &&
      bfeId.length === 2 + na.crypto_scalarmult_SCALARBYTES // = 34 bytes
    )
  } catch (err) {
    return false
  }
}

module.exports = {
  toBuffer,
  isPOBoxId,
  isBuffer: Buffer.isBuffer,
  isString: (str) => typeof str === 'string',
  isObject (obj) {
    if (obj === null) return false
    if (Buffer.isBuffer(obj)) return false
    return typeof obj === 'object'
  },
  isSameKey (A, B) {
    return toBuffer(A).equals(toBuffer(B))
  }
}
